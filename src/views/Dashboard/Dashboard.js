import React, {Component} from 'react';
import { Grid } from '@material-ui/core';
import moment from 'moment';

import {
  TotalUsers,
  LatestSales,
  UsersByDevice,
} from './components';


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      user: [],
      dataUser: {},
      dataRange: {},
    };
  }

  componentDidMount() {
    fetch('https://apinext-ms.cortilia.it/api/monitoring/api/v1/users/online?s=202003270000&e=202004032359')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            user: result.numUsers,
            dataUser: result,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )

    const sevenDays = moment().subtract(7,'d').format('YYYYMMDDHHmm');

    const today = moment().format('YYYYMMDDHHmm');



    fetch(`https://apinext-ms.cortilia.it/api/monitoring/api/v1/users/online?s=${sevenDays}&e=${today}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            dataRange: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            error
          });
        }
      )
  }

  render() {
    const { dataUser, user, dataRange } =this.state;
    return (
      <div className="wrapper_Dashboard">
        <Grid
          container
          spacing={4}
        >
          {/*  <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <Budget/>
          </Grid>*/}
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TotalUsers user={user}/>
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            {/*  <TotalProfit/> */}
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}
          >
            <LatestSales dataRange={dataRange}/>
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}
          >
            <UsersByDevice dataUser={dataUser}/>
          </Grid>
          {/* }
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}
          >
            <LatestOrders/>
          </Grid> */}
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
